
/**
* Theme: SimpleAdmin Template
* Author: Coderthemes
* Main Js File
*
*/


(function ($) {

    'use strict';

    function initSlimscroll() {
        $('.slimscroll').slimscroll({
            height: 'auto',
            position: 'right',
            size: "5px",
            color: '#9ea5ab'
        });
    }

    function initMetisMenu() {
        //metis menu
        $("#side-menu").metisMenu();
    }

    function initLeftMenuCollapse() {
        // Left menu collapse
        $('.button-menu-mobile').on('click', function (event) {
            event.preventDefault();
            $("body").toggleClass("nav-collapse");
        });
    }

    function initComponents() {
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover();

        $('[data-plugin="switchery"]').each(function (idx, obj) {
            new Switchery($(this)[0], $(this).data());
        });
    }

    function initActiveMenu() {
        // === following js will activate the menu in left side bar based on url ====
        $("#side-menu a").each(function () {
            if (this.href == window.location.href) {
                $(this).addClass("active");
                $(this).parent().addClass("active"); // add active to li of the current link
                $(this).parent().parent().prev().addClass("active"); // add active class to an anchor
                $(this).parent().parent().prev().click(); // click the item to make it drop
            }
        });
    }

    function initLoggedInUser(){
        $.get('/api/v1/secure/keycloak')
                .done(function (result) {
                    $('#username').html(result);
                })
                .error(function (err) {
                    console.log('error occured', err.status)
                });

        $.get('/api/v1/secure/super-admin')
                .done(function (result) {
                    $('#role').html(result);
                })
                .error(function (err) {
                    console.log('error occured', err.status)
                });
    }

    function init() {
        $('#topbar').load('/fragment/admin/topbar.html');
        $('#footer').load('/fragment/admin/footer.html');

        $('#sidenav').load('/fragment/admin/sidenav.html',function (){
          initMetisMenu();
          initLeftMenuCollapse();
          initActiveMenu();
          initLoggedInUser();
        });

        initSlimscroll();
        initComponents();
    }

    init();

})(jQuery)
